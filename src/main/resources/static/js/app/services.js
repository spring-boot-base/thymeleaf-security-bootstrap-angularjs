(function(angular) {

    var ProveedorFactory = function($resource) {

        return $resource('/proveedores/:proveedorId', {
            proveedorId: '@proveedorId'
        }, {
            update: {
                method: "PUT"
            },
            remove: {
                method: "DELETE"
            }
        });
    };

    ProveedorFactory.$inject = ['$resource'];
    angular.module("servicerestApp.services").factory("Proveedor", ProveedorFactory);

}(angular));